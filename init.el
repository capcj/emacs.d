(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                                    (not (gnutls-available-p))))
              (proto (if no-ssl "http" "https")))
   (when no-ssl
        (warn "\
         Your version of Emacs does not support SSL connections,
         which is unsafe because it allows man-in-the-middle attacks.
         There are two things you can do about this warning:
         1. Install an Emacs version that does support SSL and be safe.
         2. Remove this warning from your init file so you won't see it again."))
     ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
       (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
         ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
           (when (< emacs-major-version 24)
                ;; For important compatibility libraries like cl-lib
                    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))
(package-initialize)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(js-indent-level 2)
 '(package-selected-packages
   (quote
    (js2-mode company indent-tools sql-indent emojify volume aumix-mode pug-mode lsp-typescript typescript-mode ## ts-comint tide whitespace-cleanup-mode drag-stuff ede-php-autoload column-enforce-mode dumb-jump projectile move-text simpleclip helm web-mode php-mode dracula-theme))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(load-theme 'dracula t)

;; Put columns counter
(setq column-number-mode t)

;; Put lines counter
(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;; Unset C-z function
(global-unset-key (kbd "C-z"))

;; LINES MANIPULATION
;; Duplicate row
(defun duplicate-line()
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (newline)
  (yank)
  )
(global-set-key (kbd "C-c C-d") 'duplicate-line)

;; Move text package
(move-text-default-bindings)

;; store all backup and autosave files in the tmp dir
(setq backup-directory-alist `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))
(setq backup-by-copying t)
(setq delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t)

;; Projectile package to vcs project management
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(setq projectile-project-search-path '("~/dev/" "~/personaldev/"))
(define-key global-map (kbd "<f7>") 'projectile-switch-project)

;; dump-jump package to jump to definition on language
(dumb-jump-mode)
;; dumb-jump package keybinding to go to definition
(global-set-key (kbd "<f12>") 'dumb-jump-go)

;; column-enforce-mode package to ensure 80 columns
(global-column-enforce-mode t)

;; Quickly access init.el
(defun go-to-init-file ()
  "Edit the `user-init-file', in another window"
  (interactive)
  (find-file-other-window user-init-file))

;; Move better between windows
(defun prev-window ()
  (interactive)
  (other-window -1))

(global-set-key (kbd "C-,") #'prev-window)

;; drag-stuff package
(global-set-key (kbd "C-c <up>") 'drag-stuff-up)
(global-set-key (kbd "C-c <down>") 'drag-stuff-down)

;; Show whitespace
(setq-default show-trailing-whitespace t)

;;rename file and buffer
(defun rename-file-and-buffer ()
  "Rename the current buffer and file it is visiting."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
	(message "Buffer is not visiting a file!")
      (let ((new-name (read-file-name "New name: " filename)))
        (cond
	 ((vc-backend filename) (vc-rename-file filename new-name))
	 (t
	  (rename-file filename new-name t)
	  (set-visited-file-name new-name t t)))))))
(global-set-key (kbd "C-c r")  'rename-file-and-buffer)

;; Copy File Path by Buffer
(defun copy-file-path (&optional @dir-path-only-p)
"Copy the current buffer's file path or dired path to `kill-ring'.
Result is full path.
If `universal-argument' is called first, copy only the dir path.

If in dired, copy the file/dir cursor is on, or marked files.

If a buffer is not file and not dired, copy value of `default-directory' (which is usually the “current” dir when that buffer was created)

URL `http://ergoemacs.org/emacs/emacs_copy_file_path.html'
Version 2017-09-01"
  (interactive "P")
  (let (($fpath
         (if (string-equal major-mode 'dired-mode)
             (progn
               (let (($result (mapconcat 'identity (dired-get-marked-files) "\n")))
                 (if (equal (length $result) 0)
                     (progn default-directory )
                   (progn $result))))
           (if (buffer-file-name)
               (buffer-file-name)
             (expand-file-name default-directory)))))
    (kill-new
     (if @dir-path-only-p
         (progn
           (message "Directory path copied: 「%s」" (file-name-directory $fpath))
           (file-name-directory $fpath))
       (progn
         (message "File path copied: 「%s」" $fpath)
         $fpath )))))

;; TypeScript Tide Mode
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (flycheck-mode +1)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1)
  (global-set-key (kbd "C-c ,")  'tide-jump-to-definition)
  (global-set-key (kbd "C-c .")  'tide-jump-back)
  ;; company is an optional dependency. You have to
  ;; install it separately via package-install
  ;; `M-x package-install [ret] company`
  (company-mode +1))

;; Tide Format Options
;; https://github.com/Microsoft/TypeScript/blob/v3.3.1/src/server/protocol.ts#L2858-L2890
(setq tide-format-options
      '(:indentSize 2
	:tabSize 2
	:convertTabsToSpaces t
	:insertSpaceAfterFunctionKeywordForAnonymousFunctions t
	:insertSpaceAfterCommaDelimiter t
	:insertSpaceAfterOpeningAndBeforeClosingNonemptyBraces t
	:insertSpaceAfterOpeningAndBeforeClosingTemplateStringBraces nil
	:insertSpaceAfterOpeningAndBeforeClosingJsxExpressionBraces nil
	:insertSpaceAfterTypeAssertion t
	:placeOpenBraceOnNewLineForFunctions nil
	:insertSpaceAfterConstructor nil))

(setq-default typescript-indent-level 2)

;; aligns annotation to the right hand side
(setq company-tooltip-align-annotations t)

;; formats the buffer before saving
(add-hook 'before-save-hook 'tide-format-before-save)

(add-hook 'typescript-mode-hook #'setup-tide-mode)
;; /TypeScript Tide Mode

;; Add emojify to startup
(add-hook 'after-init-hook #'global-emojify-mode)
